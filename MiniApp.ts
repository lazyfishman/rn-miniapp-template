import {NativeModules} from 'react-native';
const {RNMiniApp} = NativeModules;

async function exitMiniApp() {
    return RNMiniApp.exitMiniApp();
}


export default {exitMiniApp};
